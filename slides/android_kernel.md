<!-- $theme: default -->

Android Kernel
===

![Linux Kernel](images/linux_android.png)

---

# Android kernel
- Android Kernel Common, includes changes required for Android 
- Currently maintained by Google:
    - Linux 4.9
    - Linux 4.14
    - Linux 4.19

---

# Wakelocks

---

# Power management basics
- Every CPU has a few states of power consumption, from being almost completely off, to working at full capacity.
- These different states are used by the Linux kernel to save power when the system is run
- For example, when the lid is closed on a laptop, it goes into “suspend”, which is the most power conservative mode of a device, where almost nothing but the RAM is kept awake 
- While this is a good strategy for a laptop, it is not necessarily good for mobile devices
- For example, you don’t want your music to be turned off when the screen is

---

# Wakelocks
- Android’s answer to these power management constraints is wakelocks
- The main idea is instead of letting the user decide when the devices need to go to sleep, the kernel is set to suspend as soon and as often as possible.
- In the same time, Android allows applications and kernel drivers to voluntarily prevent the system from going to suspend, keeping it awake (thus the name wakelock).
- This implies to write the applications and drivers to use the wakelock API.

---

# Wakelocks API
- Kernel Space API
```c
#include <linux/wakelock.h>
void wake_lock_init(struct wakelock *lock,
                    int type,
                    const char *name);
void wake_lock(struct wake_lock *lock);
void wake_unlock(struct wake_lock *lock);
void wake_lock_timeout(struct wake_lock *lock,
                       long timeout);
void wake_lock_destroy(struct wake_lock *lock);
```
- User-Space API
```sh
$ echo foobar > /sys/power/wake_lock
$ echo foobar > /sys/power/wake_unlock
```

---

# Wakelocks in vanilla kernel

- Since version 3.5, two features were included in the kernel to implement opportunistic suspend:
    - autosleep is a way to let the kernel trigger suspend or hibernate whenever there are no active wakeup sources.
    - wake locks are a way to create and manipulate wakeup sources from user space. The interface is compatible with the android one.

---

# Binder
- RPC/IPC mechanism
- Takes its roots from BeOS and the OpenBinder project, which some of the current Android engineers worked on
- Adds remote object invocation capabilities to the Linux Kernel
- One of the very basic functionalities of Android. Without it, Android cannot work.
- Every call to the system servers go through Binder, just like every communication between applications, and even communication between the components of a single application.

---

# Binder

---

# Anonymous Shared Memory (ashmem)

---
# Shared memory mechanism in Linux
- Shared memory is one of the standard IPC mechanisms present in most OSes
- Under Linux, they are usually provided by the POSIX SHM mechanism, which is part of the System V IPCs
- ndk/docs/system/libc/SYSV-IPC.html illustrates all the love Android developers have for these
- The bottom line is that they are flawed by design in Linux, and lead to code leaking resources, be it maliciously or not

---

# Ashmem
- Ashmem is the response to these flaws
- Notable differences are:
    - Reference counting so that the kernel can reclaim resources which are no longer in use
    - There is also a mechanism in place to allow the kernel to shrink shared memory regions when the system is under memory pressure.
- The standard use of Ashmem in Android is that a process opens a shared memory region and share the obtained file descriptor through Binder.

---

# Alarm Timers

---

# The alarm driver
- Once again, the timer mechanisms available in Linux were not sufficient for the power management policy that Android was trying to set up
- High Resolution Timers can wake up a process, but don’t fire when the system is suspended, while the Real Time Clock can wake up the system if it is suspended, but cannot wake up a particular process.
- Developed the alarm timers on top of the Real Time Clock and High Resolution Timers already available in the kernel
- These timers will be fired even if the system is suspended, waking up the device to do so
- Obviously, to let the application do its job, when the application is woken up, a wakelock is grabbed

---

# The ION Memory Allocator

---

# ION
- ION was introduced with Ice Cream Sandwich (4.0) version of Android
- Its role is to allocate memory in the system, for most of the possible cases, and to allow different devices to share buffers, without any copy, possibly from an user space application
- It’s for example useful if you want to retrieve an image from a camera, and push it to the JPEG hardware encoder from an user space application
- The usual Linux memory allocators can only allocate a buffer that is up to 512 pages wide, with a page usually being 4kiB.
- There was previously for Android (and Linux in general) some vendor specific mechanism to allocate larger physically contiguous memory areas (nvmap for nVidia, CMEM for TI, etc.

---

# ION
- ION is here to unify the interface to allocate memory in the system, no matter on which SoC you’re running on.
- It uses a system of heaps, with Linux publishing the heaps available on a given system.
- By default, you have three different heaps:
    - system Memory: virtually contiguous memory, backed by vmalloc
    - system contiguous: Physically contiguous memory, backed by kmalloc
    - carveout Large physically contiguous memory, preallocated at boot
- It also has a user space interface so that processes can allocate memory to work on.

---

# Comparison with mainline equivalents

- ION has entered staging since 3.14. And:
- The contiguous allocation of the buffers is done through CMA
- The buffer sharing between devices is made through dma-buf
- Its user space API also allows to allocate and share buffers from the user space, which was not possible otherwise.

---

# Various additions
- Android also has a lot of minor features added to the Linux kernel:
- RAM Console, a RAM-based console that survives a reboot to hold kernel logs
- pmem, a physically contiguous memory allocator, written specifically for the Qualcomm MSM SoCs. Obsolete Now.
- ADB
- YAFFS2
- Timed GPIOs

---

# Linux Mainline Patches Merge

- As of 4.14, the following patches/features are now found in the mainline kernel:
	- Binder
	- Alarm Timers (under the name POSIX Alarm Timers introduced in 2.6.38)
	- Ashmem
	- Klogger
	- Timed GPIOs
	- Low Memory Killer
	- RAM Console (superseded by pstore RAM backend introduced in 3.5)
	- ION Memory Allocator

---

# Linux Mainline Patches Merge

- As of 3.10, the following patches/features are missing from the mainline kernel:
    - Paranoid Networking
    - USB Gadget
    - FIQ debugger

- More details about upstream state:
<https://fr.slideshare.net/linaroorg/hkg18211-android-common-kernel-and-out-of-mainline-patchset-status>
