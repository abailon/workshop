<!-- $theme: default -->

Fastboot
===

![Fastboot mode](images/android_fastboot_mode.jpg)

---

# Fastboot

- Fastboot is a protocol to communicate with bootloaders over USB
- It is very simple to implement, making it easy to port on both new devices and on host systems
- Accessible with the fastboot command

---

# Fastboot protocol
- It is very restricted, only 10 commands are defined in the protocol specifications
- It is synchronous and driven by the host
- Allows to:
    - Transmit data
    - Flash the various partitions of the device
    - Get variables from the bootloader
    - Control the boot sequence

---

# Booting into Fastboot
- Usually, it’s not implemented, disabled or locked by default
- On devices that support it, you have several options:
    - Use a combination of keys at boot to start the bootloader right away into its fastboot mode
    - Using the serial console, interrupt the bootloader and run the `fastboot` command. The device will run in fastboot mode.
    - Use the `adb reboot bootloader` or `adb reboot fastboot` command on your workstation. The device will reboot in fastboot mode.
- You can then interact with the device through the fastboot command on your workstation

---

# Major Fastboot Commands
- You can get all the commands through fastboot -h
- The most widely used commands are:
    - devices: Lists the fastboot-capable devices
    - boot: Downloads a kernel and boots on it
    - erase: Erases a given flash partition name
    - flash: Writes a given file to a given flash partition
    - getvar: Retrieves a variable from the bootloader
    - continue: Goes on with a regular boot

---

# getvar Variables
- Vendor-specific variables must also begin with a upper-case letter. Variables beginning with a lower-case letter are reserved for the Fastboot specifications and their evolution.
    - version: Version of the Fastboot protocol implemented
    - version-bootloader: Version of the bootloader
    - version-baseband: Version of the baseband firmware
    - product: Name of the product
    - serialno: Product serial number
    - secure: Does the bootloader require signed images?
