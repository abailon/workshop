<!-- $theme: default -->

Android HAL
===

---

# Hardware Abstraction Layers
- Usually, the kernel already provides a HAL for user space
- However, from Google’s point of view, this HAL is not sufficient and suffers some restrictions, mostly:
- Depending on the subsystem used in the kernel, the user space interface differs
- All the code in the kernel must be GPL-licensed
- Google implemented its HAL with dynamically loaded user space libraries

---

# Library naming
- It follows the same naming scheme as for init: the generic implementation is called `libfoo.so` and the hardware-specific one `libfoo.hardware.so`
- The name of the hardware is looked up with the following properties:
	- `ro.hardware`
	- `ro.product.board`
	- `ro.board.platform`
	- `ro.arch`
- The libraries are then searched for in the directories:
	- `/vendor/lib/hw`
	- `/system/lib/hw`

---

# Various layers
- Audio (`libaudio.so`) configuration, mixing, noise cancellation, etc.
	- `hardware/libhardware/include/audio.h`
- Graphics (`gralloc.so`, `hwcomposer.so`, `libhgl.so`) handles graphic memory buffer allocations, OpenGL implementation, etc.
	- `libhgl.so` should be provided by your vendor
	- `hardware/libhardware/include/gralloc.h`
	- `hardware/libhardware/include/hwcomposer.h`
- Camera (`libcamera.so`) handles the camera functions: autofocus, take a picture, etc.
	- `hardware/libhardware/include/camera{2,3}.h`
- GPS (`libgps.so`) configuration, data acquisition
	- hardware/libhardware/include/hardware/gps.h

---

# Various layers
- Lights (`liblights.so`) Backlight and LEDs management
	- `hardware/libhardware/include/lights.h`
- Sensors (`libsensors.so`) handles the various sensors on the device: Accelerometer, Proximity Sensor, etc.
	- `hardware/libhardware/include/sensors.h`
- Radio Interface (`libril-vendor-version.so`) manages all communication between the baseband and rild
	- You can set the name of the library with the `rild.lib` and `rild.libargs` properties to find the library
	- `hardware/ril/include/telephony/ril.h`
- Bluetooth (`libbluetooth.so`) Discovery and communication with Bluetooth devices
	- `hardware/libhardware/include/bluetooth.h`
- NFC (`libnfc.so`) Discover NFC devices, communicate with it, etc.
	- `hardware/libhardware/include/nfc.h`

---

# HAL interface definition language (HIDL)

- Android 8.0 re-architected the Android OS framework (in a project known as Treble)
	- Make easier, faster, and less costly for manufacturers to update devices to a new version of Android.
	- Allow the Android framework to be replaced without rebuilding the HALs. 

- HIDL separates the vendor implementation from the Android OS framework.
- Vendors build HALs once and place them in a `/vendor` partition on the device;
- The framework, in its own partition, can then be replaced with an over-the-air (OTA) update without recompiling the HALs.

---

# HAL interface definition language (HIDL)
- In Android 7.x and earlier:
	- no formal vendor interface exists
	- Device makers must update large portions of the Android code
![](images/treble_blog_before.png)
- In Android 8.0 and higher:
	- New stable vendor interface
	- Provides access to the hardware-specific parts of Android
![](images/treble_blog_after.png)

---


# HAL interface definition language

- Android O re-architects the Android OS to define clear interfaces between the device-independent Android platform and device- and vendor-specific code.
- HIDL replaces the HAL interfaces with stable, versioned interfaces, which can be client- and server-side HIDL interfaces in C++ or Java.

---

# HAL interface definition language

- Interoperability
	- Interoperable interfaces between processes
	- May be compiled with various architectures, toolchains, and build configurations
	- Versioned and cannot be changed after they are published
- Efficiency
	- Try to minimize the number of copy operations
	- Shared memory interfaces 
	- Fast Message Queue (FMQ)
- Intuitive
	- Only in parameters for RPC
	- Values that cannot be returned from methods are returned via callback functions.
	  Data needs to persist only for the duration of the called function and may be destroyed immediately after the called function returns.

---

# HAL interface definition language

![110%](images/treble_cpp_legacy_hal_progression.png)
