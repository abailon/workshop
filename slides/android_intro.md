<!-- $theme: default -->

Android Introduction
===

---

# Presentation of Android
- Android is an open source software stack
- Supports a wide array of devices with different form factors
- Open software platform available for carriers, OEMs, and developers

---

# Features
- Application ecosystem, allowing to easily add and remove applications and publish new features across the entire system
- Support all the web technologies, with a browser built on top of the well-established Blink rendering engine
- Support hardware accelerated graphics through OpenGL ES 
- Support all the common wireless mechanisms: GSM, CDMA, UMTS, LTE, Bluetooth, WiFi, NFC

---

# Early Years
- Began as a start-up in Palo Alto, CA, USA in 2003
- Focused from the start on software for mobile devices
- Very secretive at the time, even though founders achieved a lot in the targeted area before founding it
- Finally bought by Google in 2005

---

# Opening Up
Google announced the Open Handset Alliance in 2007, a consortium of major actors in the mobile area built around Android
- Hardware vendors: Intel, Texas Instruments, Qualcomm, Nvidia, etc...
- Software companies: Google, eBay, etc...
- Hardware manufacturers: Motorola, HTC, Sony Ericsson, Samsung, etc...
- Mobile operators: T-Mobile, Telefonica, Vodafone, etc...

---

# Android Open Source Project (AOSP)
- At every new version, Google releases its source code through this project so that community and vendors can work with it
- One can fetch the source code and contribute to it, even though the development process is very locked by Google
- Only a few devices are supported through AOSP though, only the most recent Android development phones and tablets (Nexus and Pixel brand) and the Hikey

---

# Android Open Source Project (AOSP)
- AOSP project is available at http://source.android.com
  - source code
  - resources such as technical details
- Source code is split into several Git repositories
- Easily browseable using http://androidxref.com/9.0.0_r3/xref/

---

# Android Releases
- Each new version is given a dessert name
- Released in alphabetical order
- Latest releases:
  - 2.3 Gingerbread
  - 3.X Honeycomb
  - 4.0 Ice Cream Sandwich
  - 4.1/4.2/4.3 Jelly Bean
  - 4.4 KitKat
  - 5.0 / 5.1 Lollipop
  - 6.0 Marshmallow
  - 7.0 / 7.1 Nougat
  - 8.0 / 8.1 Oreo
  - 9.0 Pie
