<!-- $theme: default -->

Android: Create an application
===

![Android Studio](images/QnA/studio-editor.png)

---

# Android Studio

![Android Studio](images/QnA/studio-homepage-hero.jpg)

---

# Android Studio

- <https://developer.android.com/studio>
- <https://developer.android.com/training/basics/firstapp>

---

# Activity lifecycle

![https://developer.android.com/guide/components/activities/activity-lifecycle](images/QnA/activity_lifecycle.png)

---

# Android NDK

- Native Development Kit (NDK)
    - Compile, link, package C & C++
    - Using LLVM libc++: C++11, C++14, ~C++17

![CPP Project](images/QnA/cpp_project.png)

