<!-- $theme: default -->

Android Licenses
===

---

# Licenses
- Mostly two kind of licenses:
  - GPL/LGPL Code: Linux
  - In the external folder, it depends on the component
  - Apache/BSD: All the rest
- Google’s apps for Android, like Google Play Store are proprietary and you need to be approved by Google to get them.

---
