# Building a `boot.img`
The `boot.img` is an image that essentially contains the kernel, and depending on the configuration, a ramdisk.
Although the `boot.img` is automatically built with the `make` command,
we are going to build it manually, to start to experiment with Android.
This will let us more time to make full build later.

## Set up environment
Initialize the environment with the envsetup.sh script:
```sh
cd ~/aosp
source build/envsetup.sh
```

## Choose a target
Choose which target to build with lunch. The exact configuration can be passed as an argument.
Let's start with building an image for {{board_name}}:
```sh
lunch {{device_name}}-userdebug
```

## Building a `boot.img`
To build a `boot.img`:
```sh
make bootimage
```

This should produce `out/target/product/{{device_name}}/boot.img`.
