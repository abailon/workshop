<!-- $theme: default -->

Android Source Code
===

---

# Source Code organization
- `bionic/` is where Android’s standard C library is stored
- `bootable/` contains code samples regarding the boot of an Android device. In this folder, you will find the protocol used by all Android bootloaders and a recovery image
- `build/` holds the core components of the build system
- `cts/` The Compatibility Test Suite
- `dalvik/` contains the source code of the Dalvik virtual machine

---

# Source Code organization
- `development/` holds the development tools, debug applications, API samples, etc
- `device/` contains the device-specific components
- `docs/` contains HTML documentation hosted at http://source.android.com
- `external/` is one of the largest folders in the source code, it contains all the external projects used in the Android code
- `frameworks/` holds the source code of the various parts of the framework
- `hardware/` contains all the hardware abstraction layers

---

# Source Code organization
- `libcore/` is the Java core library
- `libnativehelper/` contains a few JNI helpers for the Android base classes
- `ndk/` is the place where you will find the Native Development Kit, which allows to build native applications for Android
- `packages/` contains the standard Android applications
- `prebuilt/` holds all the prebuilt binaries, most notably the toolchains
- `sdk/` is where you will find the Software Development Kit
- `system/` contains all the basic pieces of the Android system: init, shell, the volume manager, etc.

