# Exploring the source tree

## AndroidXRef 
While repo is fetching the source tree, we can explore the sources using
[AndroidXRef](http://androidxref.com/). [AndroidXRef](http://androidxref.com/)
is an useful website that allows us to search and read the sources of AOSP.

## Source tree fetched by repo
Android come with a couple of sources to make search and explore the sources.
To use them, we must source the Android build environment[^not_permanent_env]:
```sh
cd ~/aosp
source build/envsetup.sh
```

This gives us access to many useful tools:

- `cgrep <PATTERN> Greps on all local C/C++ files.`
- `ggrep <PATTERN> Greps on all local Gradle files.`
- `jgrep <PATTERN> Greps on all local Java files.`
- `resgrep <PATTERN> Greps on all local res/*.xml files.`
- `mangrep <PATTERN> Greps on all local AndroidManifest.xml files.`
- `mgrep <PATTERN> Greps on all local Makefiles files.`
- `sepgrep <PATTERN> Greps on all local sepolicy files.`
- `sgrep <PATTERN> Greps on all local source files.`
- `godir <filename> Go to the directory containing a file`
