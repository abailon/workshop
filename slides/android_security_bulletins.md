<!-- $theme: default -->

Android Security Bulletins
===

![](images/security/security_matters.jpg)

---

# Android Security Bulletins

- Android Security Bulletins <https://source.android.com/security/bulletin>
	- Provide fixes for security vulnerabilities impacting Android devices
	- Released monthly (on 1st and 5th)
        - Available to the public on that date
        - Available to partners 1 month prior (i.e. 5th of month-1)
	- Released on AOSP's git servers 24–48 hours after the security bulletin is released

---

# Android Security Bulletins (AOSP)

![July 2019 ASB - System](images/security/asb_july_2019_system.png)

---

# Android Security Patch levels

- Before Android 9
- Android property: `ro.build.version.security_patch`
- Find patch level
    - `getprop ro.build.version.security_patch`
    - Format: YYYY-MM-01 & YYYY-MM-05
        - 2019-07-01 & 2019-07-05

```
Devices that use the security patch level of YYYY-MM-DD or newer
must include all applicable patches in this (and previous) security
bulletins.
```

---

# Android Security Bulletins Flow

![ASB Flow](images/security/asb-flow.png)

---

# Android Security Patch levels

- Android 9 onward
- Patch version information
    - `VENDOR_PATCH_LEVEL`: vendor partition
    - `BOOT_PATCH_LEVEL`: boot partition
    - `OS_PATCH_LEVEL` & `OS_VERSION`: system partition
- Android properties
    - `ro.build.version.security_patch`
    - `ro.vendor.build.version.security_patch`

---

# Android Security Bulletins Sources

- Android device and chipset manufacturers also publish security bulletins
    - Qualcomm\
    <https://www.qualcomm.com/company/product-security/bulletins>
    - Mediatek\
    <https://www.mediatek.com/security-contact>

---

# Android Security Bulletins (Kernel)

- Kernel <https://www.kernel.org/>
    - Code snippets are provided in the ASB
    - LTS, backports & hardware vulnerabilities
        - Linked to the SoC provider/vendor
        - example: Spectre, KPTI

---

Questions ?
