<!-- $theme: default -->

Android SELinux
===

---

# SELinux

- Android uses Security-Enhanced Linux (SELinux)
- Enforce mandatory access control (MAC) over all processes
	- Even root/superuser privileges processes
- Better protect and confine system services
- Control access to application data and system logs
- Reduce the effects of malicious software
- Protect users from potential flaws in code on mobile devices

---

# SELinux

Operates on the principle of default denial: Anything not explicitly allowed is denied. SELinux can operate in two global modes:

- Permissive mode, in which permission denials are logged but not enforced.
- Enforcing mode, in which permissions denials are both logged and enforced.

In enforcing mode:
- Disallowed actions are prevented
- Dttempted violations are logged by the kernel to dmesg and logcat

---

# SELinux Permissive mode

- In permissive mode, denials are logged but not enforced
	- Ensures that policy bringup does not delay other early device bringup tasks
 	- An enforced denial may mask other denials
 	- Permissive mode ensures all denials are seen

- To put a device into permissive mode:
	- Add `androidboot.selinux=permissive` to kernel command line
	- Use `adb shell setenforce 0` to go in permissive mode until next reboot

---

# Implementation

Denials generated by core services are typically addressed by file labeling. For example:
```
avc: denied { open } for pid=1003 comm=”mediaserver” path="/dev/kgsl-3d0”
dev="tmpfs" scontext=u:r:mediaserver:s0 tcontext=u:object_r:device:s0
tclass=chr_file permissive=1
avc: denied { read write } for pid=1003 name="kgsl-3d0" dev="tmpfs"
scontext=u:r:mediaserver:s0
tcontext=u:object_r:device:s0 tclass=chr_file permissive=1
```
Boot up the system in permissive and see what denials are encountered on boot:
```
adb shell su -c dmesg | grep denied | audit2allow -p out/target/product/BOARD/root/sepolicy
```

---

# SELinux Example
```
type dhcp, domain;
permissive dhcp;
type dhcp_exec, exec_type, file_type;
type dhcp_data_file, file_type, data_file_type;

init_daemon_domain(dhcp)
net_domain(dhcp)

allow dhcp self:capability {
 setgid setuid net_admin net_raw net_bind_service
};
allow dhcp self:packet_socket create_socket_perms;
allow dhcp self:netlink_route_socket {
 create_socket_perms nlmsg_write
};
allow dhcp shell_exec:file rx_file_perms;
allow dhcp system_file:file rx_file_perms;
allow dhcp proc_net:file write;
allow dhcp system_prop:property_service set ;
unix_socket_connect(dhcp, property, init)
```
