<!-- $theme: default -->

Android Verified Boot (AVB)
===

---

# Verified Boot
- Ensure all executed code comes from a trusted source (rather than from an attacker or corruption).
- Full chain of trust:
	- Hardware-protected root of trust
	- Bootloader
	- Boot partition
	- System, vendor, product, oem partitions
- Each stage verifies the integrity and authenticity of the next stage before handing over execution.

---

# Verified Boot
- Check for the correct version of Android with rollback protection.
- Rollback protection helps to prevent a possible exploit from becoming persistent by ensuring devices only update to newer versions of Android.

In addition to verifying the OS, Verified Boot also allows Android devices to communicate their state of integrity to the user. 

---

# Android Verified Boot (AVB)

- AVB is a reference implementation of Verified Boot
- Integrated with the Android Build System
	- Enabled by a single line
	- Generate and signe all necessary dm-verity metadata

- Provides libavb, a C library
	- can be integrated within bootloader
	- provides the root of trust
	- rollback protection metadata

---

# dm-verity

- Provides transparent integrity checking of block devices
	- Helps prevent persistent rootkits that can hold onto root privileges and compromise devices
	-  Helps Android users be sure when booting a device it is in the same state as when it was last used
- Look at a block device
	- Determine if it matches its expected configuration.
	- Use a cryptographic hash tree, for every block, there is a SHA256 hash.

---

# dm-verity
![150%](images/dm-verity-hash-table.png)

---

# dm-verity operations

- dm-verity protection lives in the kernel
	- bootloader should verify the kernel
	- assuming the kernel has been verified, the kernel can look at a block device and verify it as it is mounted
- Verifies blocks individually and only when each one is accessed
- When read into memory, the block is hashed in parallel
- The hash is then verified up the tree
- The latency introduced by this block-level verification is comparatively nominal.

---

# dm-verity operations
- If verification fails
	- Generates an I/O error
	- Appear as if the filesystem has been corrupted, as is expected.
