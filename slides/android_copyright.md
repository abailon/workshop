---

# Rights to copy
- © Copyright 2004-2019, Bootlin
  Original sources: https://github.com/bootlin/training-materials/
- © Copyright 2019, BayLibre
  sources: https://gitlab.com/abailon/workshop/

---

# License: Creative Commons Attribution - Share Alike 3.0
<https://creativecommons.org/licenses/by-sa/3.0/legalcode>

You are free:
- to copy, distribute, display, and perform the work
- to make derivative works
- to make commercial use of the work

Under the following conditions:
- Attribution. You must give the original author credit.
- Share Alike. If you alter, transform, or build upon this work, you may distribute the resulting work only under a license identical to this one.
- For any reuse or distribution, you must make clear to others the license terms of this work.
- Any of these conditions can be waived if you get permission from the copyright holder.
