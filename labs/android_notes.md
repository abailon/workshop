[^req_last_aosp]: The requirements are only to build the latest version of Android.
[^not_permanent_env]: This must be done everytime you open a new terminal. This may be added to the file ~/.bashrc to make it permanent.
[^repo_init]: The example use a local manifest to fetch AOSP from a local mirror.
[^git_cred_cache]: This example enables the cache for 1 hour, whis may still be too short to fetch Android.
[^ssh_key]: You could find an example of to setup ssh key for gitlab [here](https://docs.gitlab.com/ee/ssh/)
