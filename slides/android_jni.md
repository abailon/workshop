<!-- $theme: default -->

Java Native Interface (JNI)
===

---

# What is JNI?
- A Java framework to call and be called by native applications written in other languages
- Mostly used for:
	- Writing Java bindings to C/C++ libraries
	- Accessing platform-specific features
	- Writing high-performance sections
- It is used extensively across the Android user space to interface between the Java Framework and the native daemons
- Since Gingerbread, you can develop apps in a purely native way, possibly calling Java methods through JNI

---

# C Code
```c
#include "jni.h"
JNIEXPORT void JNICALL Java_com_example_Print_print(JNIEnv *env,
                                                    jobject obj,
                                                    jstring javaString)
{
    const char *nativeString = (*env)->GetStringUTFChars(env,
                                                         javaString,
                                                         0);
    printf("%s", nativeString);
    (*env)->ReleaseStringUTFChars(env, javaString, nativeString);
}
```

---

# JNI arguments
- Function prototypes are following the template:
```
JNIEXPORT jstring JNICALL Java_ClassName_MethodName(JNIEnv*, jobject)
```
- `JNIEnv` is a pointer to the JNI Environment that we will use to interact with the virtual machine and manipulate Java objects within the native methods
- `jobject` contains a pointer to the calling object. It is very similar to `this` in C++

---

# Types
- There is no direct mapping between C Types and JNI types
- You must use the JNI primitives to convert one to his equivalent
- However, there are a few types that are directly mapped, and thus can be used directly without typecasting:
	- `Native Type` -> `JNI Type`
	- `unsigned char` -> `jboolean`
	- `signed char` -> `jbyte`
	- `unsigned short` -> `jchar`
	- `short` -> `jshort`
	- `long` -> `jint`
	- `long long` -> `jlong`
	- `float` -> `jfloat`
	- `double` -> `jdouble`

---

# Java Code

```java
package com.example;

class Print
{
  private static native void print(String str);
  public static void main(String[] args)
  {
    Print.print("HelloWorld!");
  }

  static
  {
    System.loadLibrary("print");
  }
}
```

---

# Calling a method of a Java object from C
```c
JNIEXPORT void JNICALL Java_ClassName_Method(JNIEnv *env,
                                             jobject obj)
{
    jclass cls = (*env)->GetObjectClass(env, obj);
    jmethodID hello = (*env)->GetMethodID(env,
                                          cls,
                                          "hello",
                                          "(V)V");
    if (!hello)
        return;
    (*env)->CallVoidMethod(env, obj, hello);
}
```

---

# Instantiating a Java object from C
```c
JNIEXPORT jobject JNICALL Java_ClassName_Method(JNIEnv *env,
                                                jobject obj)
{
    jclass cls = env->FindClass("java/util/ArrayList");
    jmethodID init = env->GetMethodID(cls,
                                      "<init>",
                                      "()V");
    jobject array = env->NewObject(cls, init);
    return array;
}
```
