<!-- $theme: default -->

Repo
===

---

# Repo
- There are hundreds of Git repositories
- To avoid making it too painful, Google also created a tool: repo
- Repo aggregates these Git repositories into a single folder from a manifest file describing how to find these and how to put them together
- Also aggregates some common Git commands such as diff or status that are run across all the Git repositories
- You can also execute a shell command in each repository managed by Repo using the repo forall command

---

# Repo’s manifest
- repo relies on a git repository that will contain XML files called manifests
- These manifests give information about where to download some source code and where to store it. It can also provide additional and optional information such as a revision to use, an alternative server to download from, etc.
- The main manifests are stored in a git repo, and shared between all the users, but you can add some local manifests.
- repo will also use any XML file that is under .repo/local_manifests

---

# Manifests syntax
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
<remote name="github"
fetch="https://github.com/" />
<default remote="github" />
<project name="foo/bar" path="device/foo/bar" revision="v14.42" />
<remove-project name="foo/bar" />
</manifest>
```
