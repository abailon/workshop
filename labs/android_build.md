# Building Android

## Set up environment
Initialize the environment with the envsetup.sh script:
```sh
cd ~/aosp
source build/envsetup.sh
```

## Choose a target
Choose which target to build with lunch. The exact configuration can be passed as an argument.
Let's start with building an image for {{board_name}}:
```sh
lunch {{device_name}}-userdebug
```

## Building Android
To build a full Android, just run:
```sh
make
```

You should find the images in `out/target/product/{{device_name}}`.
