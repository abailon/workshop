<!-- $theme: default -->

Inter-Process Communication, Binder
and AIDLs
===

---

# IPCs
- On modern systems, each process has its own address space, allowing to isolate data
- This allows for better stability and security: only a given process can access its address space. If another process tries to access it, the kernel will detect it and kill this process.
- However, interactions between processes are sometimes needed, that’s what IPCs are for.

---

#IPC

- On classic Linux systems, several IPC mechanisms are used:
	- Signals
	- Semaphores
	- Sockets
	- Message queues
	- Pipes
	- Shared memory
- Android, however, uses mostly:
	- Binder
	- Ashmem and Sockets

---

# Binder
- Uses shared memory for high performance
- Uses reference counting to garbage collect objects no longer in use
- Data are sent through parcels, which is some kind of serialization
- Used across the whole system, e.g., clients connect to the window manager through Binder, which in turn connects to SurfaceFlinger using Binder
- Each object has an identity, which does not change, even if you pass it to other processes.

---

# Binder
- This is useful if you want to separate components in distinct processes, or to manage several components of a single process (i.e. Activity’s Windows).
- Object identity is also used for security. Some token passed correspond to specific permissions. Another security model to enforce permissions is for every transaction to check on the calling UID.
- Binder also supports one-way and two-way messages

---

# Binder Mechanism
![](images/binder-call-stack.png)

---

# Binder Implementation
![](images/binder-calling-aidl.png)

---

# Android Interface Definition Language (AIDL)
- Very similar to any other Interface Definition Language you might have encountered
- Describes a programming interface for the client and the server to communicate using IPCs
- Looks a lot like Java interfaces. Several types are already defined, however, and you can’t extend this like what you can do in Java:
	- All Java primitive types (`int`, `long`, `boolean`, etc.)
	- `String`
	- `CharSequence`
	- `Parcelable`
	- `List` of one of the previous types
	- `Map`

# AIDLs HelloWorld
```
package com.example.android;

interface IRemoteService {
	void HelloPrint(String aString);
}
```

---

# Parcelable Objects
- If you want to add extra objects to the AIDLs, you need to make them implement the Parcelable interface
- Most of the relevant Android objects already implement this interface.
- This is required to let Binder know how to serialize and deserialize these objects
- However, this is not a general purpose serialization mechanism. Underlying data structures may evolve, so you should not store parcelled objects to persistent storage
- Has primitives to store basic types, arrays, etc.
- You can even serialize file descriptors!

---

# Implement Parcelable Classes
- To make an object parcelable, you need to:
	- Make the object implement the Parcelable interface
	- Implement the writeToParcel function, which stores the current state of the object to a Parcel object
	- Add a static field called CREATOR, which implements the Parcelable.Creator interface, and takes a Parcel, deserializes the values and returns the object
	- Create an AIDL file that declares your new parcelable class
- You should also consider Bundles, that are type-safe key-value containers, and are optimized for reading and writing values

---

# Intents
- Intents are a high-level use of Binder
- They describe the intention to do something
- They are used extensively across Android
	- Activities, Services and BroadcastReceivers are started using intents
- Two types of intents:
	- `explicit` The developer designates the target by its name
	- `implicit` There is no explicit target for the Intent. The system will find the best target for the Intent by itself, possibly asking the user what to do if there are several matches
