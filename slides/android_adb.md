<!-- $theme: default -->

Android Debug Bridge (ADB)
===

---

# ADB
- Usually on embedded devices, debugging is done either through a serial port on the device or JTAG for low-level debugging
- This setup works well when developing a new product that will have a static system. You develop and debug a system on a product with serial and JTAG ports, and remove these ports from the final product.
- For mobile devices, where you will have application developers that are not in-house, this is not enough.
- To address that issue, Google developed ADB, that runs on top of USB, so that application developer can still have debugging and low-level interaction with a production device.

---

# Implementation
- The code is split in 3 components:
	- ADBd, the part that runs on the device,
	- ADB server, which runs on the host, acts as a proxy and manages the connection to ADBd
	- ADB clients, which also run on the host, are what is used to send commands to the device
- ADBd can work either on top of TCP or USB.
	- For USB, Google has implemented a driver using both the USB gadget and the USB composite frameworks as it implements either the ADB protocol and the USB Mass Storage mechanism.
	- For TCP, ADBd just opens a socket

---

# ADB Architecture

---

![](images/adb-arch.png)

---

# Use of ADB

---

# ADB commands: Basics
- `start-server` Starts the ADB server on the host
- `kill-server` Kills the ADB server on the host
- `devices` Lists accessible devices
- `connect` Connects to a remote ADBd using TCP port 5555 by default
- `disconnect` Disconnects from a connected device
- `help` Prints available commands with help information
- `version` Prints the version number

---

# ADB commands: Files and applications
- `disable-verity` Disable DM Verity
- `remount` Remount the read only partitions
- `push` Copies a local file to the device
- `pull` Copies a remote file from the device
- `sync` Sync build output files to device
- `install` Installs the given Android package (apk) on the device
- `uninstall` Uninstalls the given package name from the device

---

# ADB commands: Debugging
- `reboot` Reboots the device. bootloader and recovery arguments are available to select the operation mode you want to reboot to
- `reboot-bootloader` Reboots the device into the bootloader
- `root` Restarts ADBd with root permissions on the device
- `logcat` Prints the device logs. You can filter either on the source of the logs or their priority level
- `shell` Runs a remote shell with a command line interface. If an argument is given, runs it as a command and prints out the result
- `bugreport` Gets all the relevant information to generate a bug report from the device: logs, internal state of the device, etc...
- `jdwp` Lists the processes that support the JDWP protocol
- `usb` Restarts ADBd listening on USB
- `tcpip` Restarts ADBd listening on TCP on the given port

---
