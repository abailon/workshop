<!-- $theme: default -->

Performance on Android
===

![https://developer.android.com/studio/profile/cpu-profiler](images/performance/cpu_profiler_android.png)

---

# Android Bootchart

![Bootchart example](images/performance/bootchart-example.png)

---

# Android Bootchart

- Android.com documentation\
<https://source.android.com/devices/tech/perf/boot-times#bootchart>
- AOSP source documentation\
<https://android.googlesource.com/platform/system/core/+/refs/heads/master/init/README.md>

---

# Android Bootchart

- Bootchart fixed

```diff
diff --git a/init/grab-bootchart.sh b/init/grab-bootchart.sh
index 2c56698a15fa..6d2fc63f4ca5 100755
--- a/init/grab-bootchart.sh
+++ b/init/grab-bootchart.sh
@@ -17,6 +17,6 @@ for f in $FILES; do
     adb "${@}" pull $LOGROOT/$f $TMPDIR/$f 2>&1 > /dev/null
 done
 (cd $TMPDIR && tar -czf $TARBALL $FILES)
-pybootchartgui ${TMPDIR}/${TARBALL}
+bootchart ${TMPDIR}/${TARBALL}
 xdg-open ${TARBALL%.tgz}.png
 echo "Clean up ${TMPDIR}/ and ./${TARBALL%.tgz}.png when done"
```

---

# Bootchart Demo

Demo !

```bash
adb shell 'touch /data/bootchart/enabled'
adb reboot
# After reboot
$ANDROID_BUILD_TOP/system/core/init/grab-bootchart.sh
```

---

# Android Bootchart

- Kernel command line `androidboot.bootchart=120` (in milliseconds)
- `fastboot boot boot.img`

---

# Flamegraphs

![Flamegraph Example](images/performance/simpleperf-tracking.png)

---

# Flamegraphs

- Source: <http://www.brendangregg.com/FlameGraphs/cpuflamegraphs.html#Linux>
- Android specifics
    - `sed 's/perf/simpleperf'`
    - <https://android.googlesource.com/platform/prebuilts/simpleperf/+/ndk-r13-release/README.md>

---

# Flamegraphs

- Fix the CPU Freq

```bash
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies
echo interactive > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo 1286400 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
echo 1286400 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
```
- Run simpleperf

```bash
# On Android target
cd /storage/emulated/0/Documents/
simpleperf record -F 99 -p 1351 --call-graph
# On host computer
adb pull /storage/emulated/0/Documents/perf.data perf.data-test
```

---

# Flamegraphs

![Find the hot code path](images/performance/simpleperf-bluetooth-bug.png)

---

# Flamegraphs

![Code fixed](images/performance/simpleperf-bluetooth-fix.png)
