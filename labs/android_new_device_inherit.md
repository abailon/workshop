---
mustache: variables.yaml
---

# Adding a new device
There are many ways to support a new device:
- Create a new one from the scratch
- Update an existing one, to support a new one
- Inherit from an existing device

## Inheriting from a device
Using inheritance make possible to create quickly and easily a new device.
Basically, the new device will just have to inherit from another device
(let's call it the `parent`), and overrides the `parent's` properties.
If the `parent` has been correctly designed, then creating a new device is
straight forward.
In addition, this simplifies the maintenance. Any updates for the `parent`
will also be applied to the inheriting device.

## Create a new device
Let's create a new Android device for the {{board_name}}.
We are going to create it from {{parent_device_name}} device, and customize it.

Create a new folder for the device:
```
mkdir device/{{soc_vendor}}/{{device_name}}
```

In this folder, creates the following files:
- vendorsetup.sh
- AndroidProducts.mk
- {{device_name}}.mk
- BoardConfig.mk

### `vendorsetup.sh`
Update `vendorsetup.sh` to create a new target for `lunch` command:
```sh
add_lunch_combo {{device_name}}-userdebug
```

Then, source `envsetup.sh` to update the list of target suppoorted by `lunch`:
```sh
source build/envsetup.sh
```

### `AndroidProducs.mk`
Set `PRODUCT_MAKEFILES` to define the makefile used to define the device:
```
PRODUCT_MAKEFILES := $(LOCAL_DIR)/{{device_name}}.mk
```

### `{{device_name}}.mk`
All the magic will happen here.
`{{device_name}}.mk` will inherit almost everything from {{parent_device_name}},
and we will just customize it.

Inherit from our new device from {{parent_device_name}}:
```
$(call inherit-product, device/{{soc_vendor}}/{{parent_device_name}}/{{parent_device_name}}.mk)
```

In addition, we should override few variables:
```
PRODUCT_NAME := {{device_name}}
PRODUCT_DEVICE := {{device_name}}
PRODUCT_BRAND := Android
PRODUCT_MODEL := AOSP on {{board_name}}
PRODUCT_MANUFACTURER := {{soc_vendor}}
```

### `BoardConfig.mk`
Again, let's include the `BoardConfig.mk` of {{parent_device_name}}:
```
include device/{{soc_vendor}}/{{parent_device_name}}/BoardConfig.mk
```

## Build
Now that we have added the support of {{board_name}} to Android, let's build it:
```
lunch {{device_name}}-userdebug
make
```

If everything is right, this will produce the images in `out/target/product/{{device_name}}`.
