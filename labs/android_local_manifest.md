---
mustache: variables.yaml
---

# Local manifests
Local manifests are manifests that are merged with the main manifest.

## Why to use local manifests ?
A local manifest could used to replace or fetch additional git repository,
without having to change the main manifest.
This is quite useful for development, and to quickly customize AOSP.

The sources that have been fetched only include AOSP sources.
We are going to use a local manifest to fetch all sources required for {{soc_vendor}}
platforms.

## Create a local manifest
A local manifest use the same syntax as any other manifest.
It should located in `~/aosp/.repo/local_manifests`.
Create the folder and the manifest file:
```sh
mkdir -p ~/aosp/.repo/local_manifests
touch ~/aosp/.repo/local_manifests/{{soc_vendor}}.xml
```

Open `{{soc_vendor}}.xml` and add the manifest tags:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
</manifest>
```

### `remote` tag
`remote` tag is use to tell to `repo` where to fetch a git repo.
Syntax is:
```xml
<remote name="name of remote" fetch="base url of git repo" />
```

In our case, we are going to use the following remote:
```xml
  <remote  name="{{local_manifest_remote_name}}"
           fetch="{{local_manifest_url}}" />
```

### `remove-project` tag:
Because the local manifests are merged with the main manifest,
and because we may want to fetch an existing project from another git repo,
this could cause error to `repo` because a project is duplicated.
The syntax of `remove-project` is:
```xml
<remove-project path="path/of/project/to/remove" name="name of project to remove">
```
`remove-project` expects exactly the same `path` and `name` values as defined in the main manifest.

As example, if we want to a customized version of Launcher3, we may get this error:
> fatal: duplicate path packages/apps/Launcher3 in /home/alex/aosp/.repo/manifest.xml
To fix it, we must use `remove-project`:
```xml
  <remove-project path="packages/apps/Launcher3" name="platform/packages/apps/Launcher3" />
```

### `project` tag:
`project` tag is used to tell to `repo` to fetch a git repo, and where to put it.
The syntax of `project` tag:
```
  <project path="destination/path/of/git/repo" name="name_of_git_repo"
           remote="name of remote to use" revision="revision id or branch name to use" />
```

`revision` and `remote` attribute are not mandatory, but thei are usually defined
in a local manifest as the default value are not the one we want to use.
`name` attribute is appended to `fetch` attribute of `remote` tag.

Example of `project` tag:
```
  <project path="device/{{soc_vendor}}/common" name="android/device_{{soc_vendor}}_common.git"
                 remote="{{local_manifest_remote_name}}" revision="{{local_manifest_rev}}" />
```
This will fetch the git repo from `{{local_manifest_url}}/android/device_{{soc_vendor}}_common.git`,
and put it in `~/aosp/device/{{soc_vendor}}/common`.

### Update the local manifest
Let's update the local manifest.
Add the `remote` to the manifest, and the following projects:
{{local_manifest_projects}}

Don't forget to remove duplicated projects using `remove-project` tag.

### Fetch
Once the manifest has been correctly configured, fetch the sources again:
```sh
repo sync --force-sync
```
`--force-sync` is required here to remove the previously fetched source when
`remove-project` is used.
Once this has completed, you should have the following folder created:
{{local_manifest_paths}}
