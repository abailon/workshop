<!-- $theme: default -->

Android Over The Air Update
===

---

# OTA Updates
- Android devices can receive and install over-the-air (OTA) updates
- Updates are designed to upgrade
	- The underlying operating system
	- The read-only apps installed on the system partition
	- Time zone rules
	- Do not affect applications installed by the user

---

# OTA Updates
2 types of updates:
- Non-A/B system updates
- A/B (Seamless) system updates
	- Ensure a workable booting system remains on the disk during an over-the-air (OTA) update
	- Reduces the probability of a damaged device after an update

---

# Life of an OTA update
- OTA package is available for downloading
- Update downloads to a cache or data partition, and its cryptographic signature is verified
- User is prompted to install the update
- Device reboots into recovery mode
- Recovery binary is started by init
	- Command-line arguments in /cache/recovery/command
	- Verifies the cryptographic signature of the package
- Partions are updated
- Device reboots normally

---

# A/B (Seamless) system updates
A/B system updates provide the following benefits:
- Updates can occur while the system is running, without interrupting the user
- Device reboots into the updated disk partition
- After an update, rebooting takes no longer than a regular reboot
- If an OTA fails to apply, or break the boot
	- The user will not be affected
	- The user will continue to run the old OS
	- The device could re-attempt the update
- Updates can be streamed to A/B devices
	- Removing the need to download the package before installing it
- The cache partition is no longer used to store OTA update packages

---

# Life of an A/B update
- OTA package is available for downloading
- Current slot is marked as `successful`
- The `unsued` slot is marked as `unbootable`
- The OTA package is downloaded
- Partions are updated, and verified against the expected hash
- The `unsued` slot is marked as `active`
- Reboot and try to boot from the `active` slot
- The `active` slot is marked as `successful`

---

# Implementing A/B Updates
- Update bootloader to manage slots
- Configure kernel to use dm-verity
- Implement the boot control HAL
- Add `slotselect` argument to dts / fstab
- Build Android with support of A/B updates
<https://source.android.com/devices/tech/ota/ab/ab_implement>
