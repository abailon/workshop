<!-- $theme: default -->

Android Hardware Requirement
===

---

# Android Hardware Requirements
- Google produces a document updated on every Android version called the Compatibility Definition Document (CDD):
  - Provides all the information you need on the expectations Google has about what should be an Android device
  - Details both the hardware and the global behaviour of the system
  - Only mandatory if you care about the Google applications
- http://source.android.com/compatibility/android-cdd.pdf

---

# SoC requirements
- Since Android in itself is quite huge, the required hardware is quite powerful
- Android officially supports only a few architectures
  - ARM v7a and ARM v8
  - x86
  - MIPS
- A GPU with OpenGL ES or Vulkan support. Latest versions of Android require the 3D hardware acceleration

---

# Storage and RAM requirements
- RAM size: at least 512MB
- Storage capacity:
    - System image: ~1.2GB
    - Vendor image: ~256MB
    - User partition: 1GB minimum, more is recommended (2GB)
  - Total: at least 3.5GB

---

# External Peripherals
- Mandatory peripherals:
  - Display
  - Input device
- Optional peripherals:
  - Sensors
  - Touchscreen
  - USB
  - Network

---
