<!-- $theme: default -->

Service Manager and Various Services
===

---

# The first step: system_server.c
- Located in frameworks/base/cmds/system_server
- Started by Zygote through the SystemServer
- Starts all the various native services:
- SurfaceFlinger
- SensorService
- It then calls back the SystemServer object’s init2 function to go on with the initialization

---

# Java Services Initialization
- Located in `frameworks/base/services/java/com/android/server/SystemServer.java`
- Starts all the different Java services in a different thread by registering them into the Service Manager
- PowerManager, ActivityManager (also handles the ContentProviders),
  PackageManager, BatteryService, LightsService, VibratorService, AlarmManager,
  WindowManager, BluetoothService, DevicePolicyManager, StatusBarManager, InputMethodManager,
  ConnectivityService, MountService, NotificationManager, LocationManager, AudioService,
  ...
- If you wish to add a new system service, you will need to add it to one of these two parts to register it at boot time
