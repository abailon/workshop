<!-- $theme: default -->

Android Filesystem
===

---

# Filesystem organization on GNU/Linux

- On most Linux based distributions, the filesystem layout is defined by the Filesystem Hierarchy Standard
- The FHS defines the main directories and their contents
	- `/bin` Essential command binaries
	- `/boot` Bootloader files, i.e. kernel images and related stuff 
	- `/etc` Host-specific system-wide configuration files.
- Android follows an orthogonal path, storing its files in folders not present in the FHS, or following it when it uses a defined folder

---

# Filesystem organization on Android
- Instead, the two main directories used by Android are
	- `/system` Immutable directory coming from the original build. It contains native binaries and libraries, framework jar files, configuration files, standard apps, etc.
	- `/vendor`, `product` and `oem` Similar to `system`, contain native binaries and libraries that are required for a SoC, a device or a product.
	- `/data` is where all the changing content of the system are put: apps, data added by the user, data generated by all the apps at runtime, etc.
- These directories are usually mounted on separate partitions, from the root filesystem originating from a kernel RAM disk.
- Android also uses some usual suspects: `/proc`, `/dev`, `/sys`, `/etc`, `/sbin`, `/mnt` where they serve the same function they usually do

---

# /system
- `./app` All the pre-installed apps
- `./bin` Binaries installed on the system (toolbox, vold, Surfaceflinger)
- `./etc` Configuration files
- `./fonts` Fonts installed on the system
- `./framework` Jar files for the framework
- `./lib` Shared objects for the system libraries
- `./xbin` External binaries

---

# /vendor
- `./bin` Binaries installed on the system (toolbox, vold, surfaceflinger)
- `./etc` Configuration files
- `./lib` Shared objects for the system libraries
- `./modules` Kernel modules
- `./firmware` Firmware binaries

---

# Other directories
- Like we said earlier, Android most of the time either uses directories not in the FHS, or directories with the exact same purpose as in standard Linux distributions (/dev, /proc, /sys), therefore avoiding collisions.
- There are some collisions though, for /etc and /sbin, which are hopefully trimmed down
- This allows to have a full Linux distribution side by side with Android with only minor tweaks

---

# android_filesystem_config.h
- Located in system/core/include/private/
- Contains the full filesystem setup, and is written as a C header
- UID/GID
- Permissions for system directories
- Permissions for system files
- Processed at compilation time to enforce the permissions throughout the filesystem
- Useful in other parts of the framework as well, such as ADB
