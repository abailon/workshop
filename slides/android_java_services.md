<!-- $theme: default -->

Various Java Services
===

---

# Android Java Services
- There are lots of services implemented in Java in Android
- They abstract most of the native features to make them available in a consistent way
- You get access to the system services using the Context.getSystemService() call
- You can find all the accessible services in the documentation for this function

---

# ActivityManager
- Manages everything related to Android applications
	- Starts Activities and Services through Zygote
	- Manages their lifecycle
	- Fetches content exposed through content providers
	- Dispatches the implicit intents
	- Adjusts the Low Memory Killer priorities
	- Handles non responding applications

---

# PackageManager
- Exposes methods to query and manipulate already installed packages, so you can:
	- Get the list of packages
	- Get/Set permissions for a given package
	- Get various details about a given application (name, uids, etc)
	- Get various resources from the package
- You can even install/uninstall an apk
	- installPackage/uninstallPackage functions are hidden in the source code, yet public.
	- You can’t compile code that is calling directly these functions and they are not documented anywhere except in the code
	- But you can call them through the Java Reflection API, if you have the proper permissions of course

---

# PowerManager
- Abstracts the Wakelocks functionality
- Defines several states, but when a wakelock is grabbed, the CPU will always be on
	- `PARTIAL_WAKE_LOCK` Only the CPU is on, screen and keyboard backlight are off
	- `SCREEN_DIM_WAKE_LOCK` Screen backlight is partly on, keyboard backlight is off
	- `SCREEN_BRIGHT_WAKE_LOCK` Screen backlight is on, keyboard backlight is off
	- `FULL_WAKE_LOCK` Screen and keyboard backlights are on

---

# AlarmManager
- Abstracts the Android timers
- Allows to set a one time timer or a repetitive one
- When a timer expires, the AlarmManager grabs a wakelock, sends an Intent to the corresponding application and releases the wakelock once the Intent has been handled

---

#ConnectivityManager and WifiManager
- ConnectivityManager
	- Manages the various network connections
		- Falls back to other connections when one fails
		- Notifies the system when one becomes available/unavailable
		- Allows the applications to retrieve various information about connectivity
- WifiManager
	- Provides an API to manage all aspects of WiFi networks
		- List, modify or delete already configured networks
		- Get information about the current WiFi network if any
		- List currently available WiFi networks
		- Sends Intents for every change in WiFi state
