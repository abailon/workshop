# Requirements[^req_last_aosp]

Before you download and build the Android source, ensure that your system meets the following requirements, then see Establishing a Build Environment for installation instructions by operating system. 

## Hardware requirements
* A 64-bit environment is required.
* At least 250GB of free disk space to check out the code and an extra 150 GB to build it.
* At least 16 GB of RAM/swap.

## Software requirements
* [Ubuntu 16.04](http://releases.ubuntu.com/16.04/ubuntu-16.04.6-desktop-amd64.iso) is recommended.

# Establishing a Build Environment[^req_last_aosp]

## Setting up a Linux build environment

### Update the OS
It is recommended to update the OS, to prevent package manager issue:
```sh
sudo apt-get update
sudo apt-get upgrade
```

### Install required packages
```sh
sudo apt-get install git-core gnupg flex bison gperf build-essential zip \
	curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 \
	lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev \
	libgl1-mesa-dev libxml2-utils xsltproc unzip
```

## Installing Repo

Repo is a tool that makes it easier to work with Git in the context of Android.

To install it:
```sh
mkdir ~/bin
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
```
