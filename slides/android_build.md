<!-- $theme: default -->

Android Build System
===

![Gradle + Android][50%](images/gradle-android.png)

---

# envsetup.sh

---

# Purpose
- Obviously modifies the current environment, that’s why we have to source it
- It adds many useful shell macros
- These macros will serve several purposes:
	- Configure and set up the build system
	- Ease the navigation in the source code
	- Ease the development process
- Some macros will modify the environment variables, to be used by the build system later on

---

# Defined Commands
`lunch` Used to configure the build system
`croot` Changes the directory to go back to the root of the Android source tree
`cproj` Changes the directory to go back to the root of the current package
`tapas` Configure the build system to build a given application
`m` Makes the whole build from any directory in the source tree
`mm` Builds the modules defined in the current directory
`mmm` Builds the modules defined in the given directory

---

# Defined Commands
`cgrep` Greps the given pattern on all the C/C++/header files
`jgrep` Greps the given pattern on all the Java files
`resgrep` Greps the given pattern on all the resources files
`mgrep` Greps the given pattern on all the Makefiles
`sgrep` Greps the given pattern on all Android source file
`godir` Go to the directory containing the given file
`pid` Use ADB to get the PID of the given process
`gdbclient` Use ADB to set up a remote debugging session
`key_back`, `key_home` Sends a input event corresponding to the Back and Home keys to the device

---

# Environments variables exported
- `ANDROID_TOOLCHAIN` Path to the Android prebuilt toolchain
- `ANDROID_BUILD_PATHS` Path containing all the folders containing tools for the build
- `ANDROID_JAVA_HOME` Path to the Java environment
- `ANDROID_JAVA_TOOLCHAIN` Path to the Java toolchain
- `ANDROID_PRODUCT_OUT` Path to where the generated files will be for this product

---

# Configuration of the Build System

---

# Configuration
- The Android build system is not much configurable compared to other build systems, but it is possible to modify to some extent
- Among the several configuration options you have, you can add extra flags for the C compiler, have a given package built with debug options, specify the output directory, and first of all, choose what product you want to build.
- This is done either through the lunch command or through a buildspec.mk file placed at the top of the source directory
- 
---

# lunch
- `lunch` is a shell function defined in `build/envsetup.sh`
- It is the easiest way to configure a build. You can either launch it without any argument and it will ask to choose among a list of known “combos” or launch it with the desired combos as argument.
- It sets the environment variables needed for the build and allows to start compiling at last
- You can declare new combos through the `add_lunch_combo` command
- These combos are the aggregation of the product to build and the variant to use (basically, which set of modules to install)

---

# Variables Exported by Lunch
- `TARGET_PRODUCT` Product to build
- `TARGET_BUILD_VARIANT` Select which set of modules to build
	- `user`: Includes modules tagged user
	- `userdebug`: Includes modules tagged user or debug
	- `eng`: Includes modules tagged user, debug or eng
- `TARGET_BUILD_TYPE` Either `release` or `debug`.
   If `debug` is set, it will enable some debug options across the whole system.

---

# Results

---

# Output
- All the output is generated in the `out/` directory, outside of the source code directory
- This directory contains mostly two subdirectories: `host/` and `target/`
- These directories contain all the objects files compiled during the build process: `.o` files for C/C++ code, `.jar` files for Java libraries, etc
- It is an interesting feature, since it keeps all the generated stuff separate from the source code, and we can easily clean without side effects

---

# Images
- It also generates the system images in the `out/target/product/<device_name>/` directory
- These images are:
	- `boot.img` A basic Android image, containing only the needed components to boot: a kernel image and a minimal system
	- `system.img` The remaining parts of Android. Much bigger, it contains most of the framework, applications and daemons
	- `vendor.img` Vendor parts of Android. It contains the HALs, the vendor libraries and applications, the firmwares and kernel modules
	- `userdata.img` A partition that will hold the user generated content. Mostly empty at compilation.

---

# Images
- Optional images:
	- `recovery.img` A recovery image that allows to be able to debug or restore the system when something nasty happened. Only required for non A/B updates.
	- `product.img` Contains files and customization specifics to product.
	- `oem.img` Contains files and customization specifics to OEM.

---

# Cleaning
- Cleaning is almost as easy as `rm -rf out/`
- make clean or make clobber deletes all generated files.
- make installclean removes the installed files for the current combo. It is useful when you work with several products to avoid doing a full rebuild each time you change from one to the other
