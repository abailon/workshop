---
mustache: variables.yaml
---

# Downloading the Source

The Android source tree is located in a Git repository hosted by Google.
The Git repository includes metadata for the Android source,
including changes to the source and when the changes were made. 

## Initializing a Repo client
Configure Git with your real name and email address:
```sh
git config --global user.name "Your Name"
git config --global user.email "you@example.com"
```

## Downloading the Android source tree
Create an empty directory to hold your working files:
```sh
mkdir ~/aosp
cd ~/aosp
```

Before to use repo, it must be added to the path[^not_permanent_env]:
```sh
PATH=~/bin:$PATH
```

Run `repo init` to prepare the download of source tree[^repo_init]:
```sh
repo init -u {{manifest_url}}  -b {{manifest_rev}}
```

Then, fetch the source tree, go grab a coffee and take some rest, because this will take a while!
```sh
repo sync
```

## Tips

### Deal with user authentication
If you are trying to get access to private git tree, repo will ask you for a username
and a password. This could become very annoying because there are many git tree to download.

#### Htpps
One way to deal with it is to use the credential cache[^git_cred_cache]:
```sh
git config --global credential.helper cache
git config --global credential.helper 'cache --timeout=3600'
```

#### Ssh
Another way is to ssh. Repo doesn't support ssh, so we must configure git to convert use ssh instead of https:
```sh
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
```

This obviously requires to setup the git server with your ssh key[^ssh_key].
In addition, if your ssh key has a password, you should use the ssh agent:
```sh
eval `ssh-agent`
ssh-add
```
ssh-add will ask your passphrase, and store your private key into the ssh-agent.
