<!-- $theme: default -->

Android Architecture
===

---

# Architecture
# ![60%](images/android_stack_480.png)

---

# Linux Kernel
- Used as the foundation of the Android system
- Numerous additions from the stock Linux, including:
  - New IPC (Inter-Process Communication) mechanisms
  - Alternative power management mechanism
  - New drivers and various additions across the kernel
- Most of these changes have been merged into upstream Linux kernel

---

# Hardware abstraction layer (HAL)
- Defines a standard interface for hardware vendors to implement
- Enables Android to be agnostic from lower-level driver implementations
- Allows to implement functionality without affecting or modifying the higher level system
- Packaged into modules and loaded by the Android system at the appropriate time

---

# Android Runtime
Handles the execution of Android applications
- Almost entirely written from scratch by Google
- Contains Dalvik, the virtual machine that executes every application that you run on Android, and the core library for the Java runtime
- Also contains system daemons, init executable, basic binaries, etc...

---

# Native Libraries
- Gather a lot of Android-specific libraries to interact at low-level with the system, and third-parties libraries as well 
- Bionic is the C library, SurfaceManager is used for drawing surfaces on the screen, etc...
- But also Blink, SQLite, OpenSSL coming from the free software world

---

# Android Framework
- Provides an API for developers to create applications
- Exposes all the needed subsystems by providing an abstraction
- Allows to easily use databases, create services, expose data to other applications, receive system events, etc...

---

# Applications
- AOSP also comes with a set of applications such as the phone application, a browser, a contact management application, an email client, etc...
- However, Google apps and Android Market app aren’t free software, so they are not available in AOSP. To obtain them, you must contact Google and pass a compatibility test
