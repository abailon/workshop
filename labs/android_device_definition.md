# Device definition

All the product files are usually in `device/<company_name>/<device_name>`:
- `AndroidProducts.mk`
- `<device_name>.mk`
- `device.mk`
- `BoardConfig.mk`
- `vendorsetup.sh`

You may also find these ones:
- Manifest.xml

## AndroidProducts.mk
`AndroidProducts.mk` usually define which makefiles to include to build the product.

Example:
```
PRODUCT_MAKEFILES := $(LOCAL_DIR)/mt8167.mk
```

## `<device_name>.mk`
`<device_name>.mk` This includes (using inherit-product) all makefiles that defines the product.
Usually, this includes generic product makefiles, usually from `$(SRC_TARGET_DIR)/product/`.

Example:
```
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)
```

In addition, this usually defines this variables:

- `PRODUCT_NAME` End-user-visible name for the overall product. Appears in the Settings > About screen.
- `PRODUCT_DEVICE` Name of the industrial design. This is also the board name, and the build system uses it to locate the `BoardConfig.mk`.
- `PRODUCT_BRAND` The brand (e.g., carrier) the software is customized for, if any
- `PRODUCT_MODEL` End-user-visible name for the end product
- `PRODUCT_MANUFACTURER` Name of the manufacturer
Refer to <https://source.android.com/setup/develop/new-device#prod-def> for more details.

Example:
```
PRODUCT_NAME := mt8167-evb
PRODUCT_DEVICE := mt8167
PRODUCT_BRAND := Android
PRODUCT_MODEL := AOSP on mt8167-evb
PRODUCT_MANUFACTURER := mediatek
```

## device.mk
This is usually inherited by `<device_name>.mk`, and defines the product.
Usually, this defines the following variable:
- `PRODUCT_COPY_FILES` List of words like source_path:destination_path.
  The file at the source path should be copied to the destination path when building this product.
- `PRODUCT_PACKAGES`  Lists the APKs and modules to install.
  Be aware that make may not raise an error if a module is not build.
Refer to <https://source.android.com/setup/develop/new-device#prod-def> for more details.

## BoardConfig.mk
This contains board-specific configurations.
Usually, this defines the following variables:
- `TARGET_ARCH`, `TARGET_ARCH_VARIANT`, `TARGET_CPU_ABI`, `TARGET_CPU_ABI2`, `TARGET_CPU_VARIANT`
  These variables are used to configure how the binaries are going to be build
	- select the compiler to use
	- select compiler options
	- build some modules with architecture or cpu specifics code

- `TARGET_2ND_ARCH`, `TARGET_2ND_ARCH_VARIANT`, `TARGET_2ND_CPU_ABI`, `TARGET_2ND_CPU_ABI2`,
  `TARGET_2ND_CPU_VARIANT`
  Same as the previous one, but used for multilib build, e.g. to both 32 bit and 64 bit
  libraries and software.
- `BOARD_KERNEL_CMDLINE` Set the kernel command line. This goes in boot.img,
  and the bootloader will pass it to kernel at boot.
- `BOARD_SYSTEMIMAGE_PARTITION_SIZE`, `BOARD_USERDATAIMAGE_PARTITION_SIZE`,
  `BOARD_VENDORIMAGE_PARTITION_SIZE`, `BOARD_CACHEIMAGE_PARTITION_SIZE`,
  `BOARD_BOOTIMAGE_PARTITION_SIZE`, `BOARD_RECOVERYIMAGE_PARTITION_SIZE`,
  `BOARD_DTBOIMG_PARTITION_SIZE`, `BOARD_OEMIMAGE_PARTITION_SIZE`,
  `BOARD_PRODUCTIMAGE_PARTITION_SIZE`, `CUSTOM_IMAGE_PARTITION_SIZE`
  Define the size of a partition. Some partition won't be build if there size is
  not specified. In addition, this is also required by AVB, to generate metadata.

- `BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE`, `BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE`,
  `BOARD_OEMIMAGE_FILE_SYSTEM_TYPE`, `BOARD_ODMIMAGE_FILE_SYSTEM_TYPE`,
  `BOARD_SYSTEMIMAGE_FILE_SYSTEM_TYPE`, `BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE`,
  `BOARD_PRODUCTIMAGE_FILE_SYSTEM_TYPE`, `PRIVATE_FILE_SYSTEM_TYPE`
  Define the file system type used to format the partition.
  Some partitions have a default file system type, for the others, this must
  be defined.
- `BOARD_AVB_ENABLE` Enable Android Verified boot
- `BOARD_AVB_ALGORITHM` Algorithm used to generate the checksums for AVB
  See <http://androidxref.com/9.0.0_r3/xref/external/avb/avbtool#97> for more details.
- `TARGET_IS_64_BIT` Tell to build system to prefer the usage of 64 bit libraries and binaries
- `BOARD_BUILD_SYSTEM_ROOT_IMAGE` Shall be set to `true` when AVB or A/B OTA update
  are enabled. This will remove the ramdisk from `boot.img`, and merge it with `system.img`.
  Enabling this option also requires to add `skip_initramfs` to the kernel command
  line.
- `BOARD_VNDK_VERSION` Shall be set to `current` when the `Treble` enabled.
- `TARGET_COPY_OUT_VENDOR` Shall be set to `vendor` when `Treble` is enabled.
- `DEVICE_MANIFEST_FILE` This shall be defined with the path of manifest file.
  This is required when `Treble` is enabled.
- `BOARD_SEPOLICY_DIRS` Shall contains all the path of folder containing SELinux
  policies.
- `BOARD_PREBUILT_DTBOIMAGE` Define the path of a prebuilt dtbo.
  Setting this variable will cause the build system to install the dtbo in
  `out/target/product/<device_name>/`

Other variables:
- wpa_supplicant:
	- `WPA_SUPPLICANT_VERSION` Shall be set to `VER_0_8_X` to use generic wpa_supplicant
	- `BOARD_WPA_SUPPLICANT_DRIVER`, `BOARD_HOSTAPD_DRIVER`
	  Set the driver to use, usually, `NL80211`

## vendorsetup.sh
Add a target to lunch.
`source build/envsetup.sh` will look for all `vendorsetup.sh` and source them.
Example of `vendorsetup.sh`:
```sh
add_lunch_combo pumpkin-userdebug
```

## Manifest.xml
The manifest file, installed using `DEVICE_MANIFEST_FILE` tells to Android:
- the HALs installed on the device
- the version of the HALS
- the mode (`passthrough`, `hwbinder`)
More details about it here: <https://source.android.com/devices/architecture/vintf#manifest-file-schema>

## Other files
There are usually many other file:
- init (`*.rc`) files
- ftab files
- sepolicy rules
- prebuilt binaries (kernel, dtb, dtbo, etc)
- HALs modules

Because there are too much files, this hard to describe all of them.
In addition, some of them will be described later, as part of other chapters.
