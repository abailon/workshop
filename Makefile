ifeq ($(PROJECT_PATH),)
PROJECT_PATH := projects
endif

ifneq ($(PROJECT),)
OUT := out/$(PROJECT)
PROJECTS := $(PROJECT)
include $(PROJECT_PATH)/$(PROJECT).mk
else
PROJECTS := $(subst $(PROJECT_PATH)/,out/,$(subst .mk,,$(wildcard $(PROJECT_PATH)/*.mk)))
endif

out/%: $(PROJECT_PATH)/%.mk
	make PROJECT_PATH=$(PROJECT_PATH) PROJECT=$* android_labs android_slides

all: $(PROJECTS)

out:
	@mkdir out/

clean:
	@rm -r -f out/

$(OUT): out
	@mkdir -p $@

DIAGRAMS := $(wildcard diagrams/*.dia)
PNG := $(subst diagrams,images,$(DIAGRAMS:.dia=.png))

images/%.png: diagrams/%.dia
	dia -e $@ -t png $^

diagrams: $(PNG)

ANDROID_SLIDES := \
	android_header \
	agenda_thales \
	baylibre \
	android_copyright \
	android_intro \
	android_licenses \
	android_arch \
	android_hardware \
	android_repo \
	android_sources \
	android_bootloader \
	android_fastboot \
	android_kernel \
	android_build \
	android_fs \
	android_adb \
	android_performance \
	android_native \
	android_hal \
	android_jni \
	android_servicemanager \
	android_ipc \
	android_java_services \
	android_ota \
	android_verified_boot \
	android_selinux \
	android_security_bulletins \
	android_QnA \

ANDROID_SLIDES := $(addprefix slides/,$(addsuffix .md,$(ANDROID_SLIDES)))

android_slides: $(OUT) diagrams $(ANDROID_SLIDES)
	pandoc --toc -st beamer $(ANDROID_SLIDES) -V theme:BayLibre \
		-f markdown+lists_without_preceding_blankline -o $(OUT)/android_slides.pdf

ANDROID_LABS := \
	android_setup_env \
	android_fetch_sources \
	android_sources_tools \
	android_local_manifest \
	android_new_device_inherit \
	android_build_boot \
	android_build \
	android_notes \
	android_copyright \

YAML_FILES := $(addprefix labs/,$(addsuffix .yaml,general $(ANDROID_LABS)))
YAML_LAB_FILES := $(wildcard labs/*.yaml)
YAML_FILES := $(filter $(YAML_LAB_FILES),$(YAML_FILES))
YAML_FILES += $(addprefix yaml/,$(addsuffix .yaml,$(YAML_PROJECT_FILES)))
ifneq ("$(wildcard $(PROJECT_PATH)/$(PROJECT).yaml)","")
YAML_FILES += $(wildcard $(PROJECT_PATH)/$(PROJECT).yaml)
endif

$(OUT)/variables.yaml: $(OUT) $(YAML_FILES)
	@yaml-merge $(YAML_FILES) > $@

$(OUT)/%.md: labs/%.md
	@cp $^ $@

ANDROID_LABS := $(addsuffix .md,$(ANDROID_LABS))
ANDROID_LABS_OUT := $(addprefix $(OUT)/,$(ANDROID_LABS))

android_labs: $(OUT) $(OUT)/variables.yaml $(ANDROID_LABS_OUT)
	@cd $(OUT); pandoc --filter pandoc-mustache --toc $(ANDROID_LABS) \
		-f markdown+lists_without_preceding_blankline -t markdown -o android_labs.md
	@pandoc --toc $(OUT)/android_labs.md -o $(OUT)/android_labs.pdf

