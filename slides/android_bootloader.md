<!-- $theme: default -->

Android Bootloader
===

![Android Bootloader Logo](images/android_bootloader.jpeg)

---

# Bootloader
- The bootloader is a piece of code responsible for
    - Basic hardware initialization
    - Loading of an application binary, usually an operating system kernel
        - from flash storage
        - from the network
        - from another type of non-volatile storage
    - Possibly decompression of the application binary
    - Execution of the application
- Besides these basic functions, most bootloaders provide a shell with various commands implementing different operations.
  - Loading of data from storage or network, memory inspection, hardware diagnostics and testing, etc.

---

# Booting on embedded CPUs
- The CPU has an integrated boot code in ROM
    - BootROM on AT91 CPUs, “ROM code” on OMAP, etc.
    - Exact details are CPU-dependent
- This boot code is able to load a first stage bootloader from a storage device into an internal SRAM (DRAM not initialized yet)
    - Storage device can typically be: MMC, NAND, SPI flash, UART (transmitting data over the serial line), etc.
- The first stage bootloader is:
    - Limited in size due to hardware constraints (SRAM size)
    - Provided either by the CPU vendor or through community projects
- This first stage bootloader must initialize DRAM and other hardware devices and load a second stage bootloader into RAM

---

# Booting on Mediatek SoC

![](images/mediatek-boot.png)

---

# Generic bootloaders for embedded CPUs
- There are several open-source generic bootloaders. Here are the most popular ones:
    - U-Boot, the universal bootloader by Denx, the most used on ARM. The de-facto standard nowadays
    <http://www.denx.de/wiki/U-Boot>
    - LK (Little Kernel), a tiny operating system suited for small embedded devices and bootloaders. LK is used as Android bootloader, and also by Android trusty OS.
    <https://github.com/littlekernel/lk>
- There are also a lot of other open-source or proprietary bootloaders, often architecture-specific
