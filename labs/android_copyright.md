# Copyright
Portions of this page are modifications based on work created and shared by the Android Open Source Project and used according to terms described in the Creative Commons 3.0 Attribution License.

[//]: # (android_setup_env)
- <https://source.android.com/setup/build/requirements>

[//]: # (android_fetch_soruces)
- <https://source.android.com/setup/build/initializing>
- <https://source.android.com/setup/build/downloading>

[//]: # (android_build)
- <https://source.android.com/setup/build/building>
